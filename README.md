# React App Config
## @healthwise/react-app-config

React App Config has two main purposes. First, it allows you to quickly create a new React app in the "Healthwise way" with zero configuration. Second, it makes things easier in existing apps by allowing the use of Healthwise centralized React configuration and build scripts.
