// Do this as the first thing so that any code reading it knows the right env.
process.env.BABEL_ENV = 'production'
process.env.NODE_ENV = 'production'

// Makes the script crash on unhandled rejections instead of silently
// ignoring them. In the future, promise rejections that are not handled will
// terminate the Node.js process with a non-zero exit code.
process.on('unhandledRejection', err => {
  throw err
})

// Ensure environment variables are read.
require('../config/env')

const path = require('path')
const spawn = require('react-dev-utils/crossSpawn')

const result = spawn.sync(
  'node',
  [
    require.resolve('@storybook/react/bin/build.js'),
    '-c',
    path.resolve(__dirname, '..', 'config', 'storybook'),
  ],
  { stdio: 'inherit' }
)
if (result.signal) {
  if (result.signal === 'SIGKILL') {
    console.log(
      'Storybook failed because the process exited too early. ' +
        'This probably means the system ran out of memory or someone called ' +
        '`kill -9` on the process.'
    )
  } else if (result.signal === 'SIGTERM') {
    console.log(
      'Storybook failed because the process exited too early. ' +
        'Someone might have called `kill` or `killall`, or the system could ' +
        'be shutting down.'
    )
  }
  process.exit(1)
}
process.exit(result.status)
