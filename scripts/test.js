// Do this as the first thing so that any code reading it knows the right env.
process.env.BABEL_ENV = 'test'
process.env.NODE_ENV = 'test'
process.env.PUBLIC_URL = ''

// Makes the script crash on unhandled rejections instead of silently
// ignoring them. In the future, promise rejections that are not handled will
// terminate the Node.js process with a non-zero exit code.
process.on('unhandledRejection', err => {
  throw err
})

// Ensure environment variables are read.
require('../config/env')

const jest = require('jest')
const checkConfigFiles = require('../util/checkConfigFiles')
const createJestConfig = require('../util/createJestConfig')
const paths = require('../config/paths')
const options = process.argv.slice(2)

let customConfig = {}
if (checkConfigFiles([paths.jestConfig])) {
  customConfig = require(paths.jestConfig)
}

const config = createJestConfig(require(paths.ownJestConfig), customConfig)

options.push('--coverage', '--config', JSON.stringify(config))
jest.run(options)
