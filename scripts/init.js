// @remove-file-on-eject
/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

// Makes the script crash on unhandled rejections instead of silently
// ignoring them. In the future, promise rejections that are not handled will
// terminate the Node.js process with a non-zero exit code.
process.on('unhandledRejection', err => {
  throw err
})

const fs = require('fs-extra')
const path = require('path')
const chalk = require('chalk')
const spawn = require('react-dev-utils/crossSpawn')

module.exports = function(appPath, appName, verbose, originalDirectory, template) {
  const ownPackageName = require(path.join(__dirname, '..', 'package.json')).name
  const ownPath = path.join(appPath, 'node_modules', ownPackageName)
  const appPackage = require(path.join(appPath, 'package.json'))

  // Copy over some of the devDependencies
  appPackage.dependencies = appPackage.dependencies || {}

  // Setup the script rules
  appPackage.scripts = {
    start: 'react-app-config start',
    'build-dev': 'react-app-config build-dev --watch',
    'build-prod': 'react-app-config build-prod',
    test: 'react-app-config test --env=jsdom',
    'start-storybook': 'react-app-config start-storybook',
    'build-storybook': 'react-app-config build-storybook',
    'check-updates': 'react-app-config check-updates',
    lint: 'react-app-config lint',
    precommit: 'lint-staged',
  }

  // Setup lint-staged rules
  appPackage['lint-staged'] = {
    '*.js': ['lint', 'git add'],
  }

  fs.writeFileSync(path.join(appPath, 'package.json'), JSON.stringify(appPackage, null, 2))

  const readmeExists = fs.existsSync(path.join(appPath, 'README.md'))
  if (readmeExists) {
    fs.renameSync(path.join(appPath, 'README.md'), path.join(appPath, 'README.old.md'))
  }

  // Copy the files for the user
  const templatePath = template
    ? path.resolve(originalDirectory, template)
    : path.join(ownPath, 'template')
  if (fs.existsSync(templatePath)) {
    fs.copySync(templatePath, appPath)
  } else {
    console.error(`Could not locate supplied template: ${chalk.green(templatePath)}`)
    return
  }

  // Rename gitignore after the fact to prevent npm from renaming it to .npmignore
  // See: https://github.com/npm/npm/issues/1862
  fs.move(path.join(appPath, 'gitignore'), path.join(appPath, '.gitignore'), [], err => {
    if (err) {
      // Append if there's already a `.gitignore` file there
      if (err.code === 'EEXIST') {
        const data = fs.readFileSync(path.join(appPath, 'gitignore'))
        fs.appendFileSync(path.join(appPath, '.gitignore'), data)
        fs.unlinkSync(path.join(appPath, 'gitignore'))
      } else {
        throw err
      }
    }
  })

  const command = 'npm'
  let args = ['install', '--save', 'react', 'react-dom']

  // Install additional template dependencies, if present
  const templateDependenciesPath = path.join(appPath, '.template.dependencies.json')
  if (fs.existsSync(templateDependenciesPath)) {
    const templateDependencies = require(templateDependenciesPath).dependencies
    args = args.concat(
      Object.keys(templateDependencies).map(key => {
        return `${key}@${templateDependencies[key]}`
      })
    )
    console.log(`Installing template dependencies using ${command}...`)
    console.log()

    const proc = spawn.sync(command, args, { stdio: 'inherit' })
    if (proc.status !== 0) {
      console.error(`\`${command} ${args.join(' ')}\` failed`)
      return
    }
    fs.unlinkSync(templateDependenciesPath)
  }

  // Install react and react-dom for backward compatibility with old CRA cli
  // which doesn't install react and react-dom along with react-app-config
  // or template is presetend (via --internal-testing-template)
  if (!isReactInstalled(appPackage) || template) {
    console.log(`Installing react and react-dom using ${command}...`)
    console.log()

    const proc = spawn.sync(command, args, { stdio: 'inherit' })
    if (proc.status !== 0) {
      console.error(`\`${command} ${args.join(' ')}\` failed`)
      return
    }
  }

  // Display the most elegant way to cd.
  // This needs to handle an undefined originalDirectory for
  // backward compatibility with old global-cli's.
  let cdpath
  if (originalDirectory && path.join(originalDirectory, appName) === appPath) {
    cdpath = appName
  } else {
    cdpath = appPath
  }

  console.log()
  console.log(`Success! Created ${appName} at ${appPath}`)

  console.log('Inside that directory, you can run several commands:')
  console.log()
  console.log(chalk.cyan('  npm start'))
  console.log(
    '    Runs the app in development mode. The page will automatically reload if you make changes.'
  )
  console.log()
  console.log(chalk.cyan('  npm run build-dev'))
  console.log('    Builds the app for development, and watches for changes.')
  console.log('    Use this to develop the app with an existing back-end, such as a .NET app.')
  console.log()
  console.log(chalk.cyan('  npm run build-prod'))
  console.log('    Builds the app for production to the build folder.')
  console.log()
  console.log(chalk.cyan('  npm test'))
  console.log('    Launches the Jest test runner.')
  console.log()
  console.log(chalk.cyan('  npm run start-storybook'))
  console.log('    Runs Storybook in development mode, and watches for changes.')
  console.log()
  console.log(chalk.cyan('  npm run build-storybook'))
  console.log('    Builds Storybook as static files for deployment.')
  console.log()
  console.log(chalk.cyan('  npm run check-updates'))
  console.log('    Checks for updates to installed packages.')
  console.log()
  console.log(chalk.cyan('  npm run lint'))
  console.log('    Lints code and fixes formatting issues using Prettier.')
  console.log()
  console.log('We suggest that you begin by typing:')
  console.log()
  console.log(`  ${chalk.cyan(`cd ${cdpath}`)}`)
  console.log(`  ${chalk.cyan('npm start')}`)
  if (readmeExists) {
    console.log()
    console.log(chalk.yellow('You had a `README.md` file, we renamed it to `README.old.md`'))
  }
  console.log()
  console.log('Happy hacking!')
}

function isReactInstalled(appPackage) {
  const dependencies = appPackage.dependencies || {}

  return (
    typeof dependencies.react !== 'undefined' && typeof dependencies['react-dom'] !== 'undefined'
  )
}
