// Do this as the first thing so that any code reading it knows the right env.
process.env.BABEL_ENV = 'development'
process.env.NODE_ENV = 'development'

// Makes the script crash on unhandled rejections instead of silently
// ignoring them. In the future, promise rejections that are not handled will
// terminate the Node.js process with a non-zero exit code.
process.on('unhandledRejection', err => {
  throw err
})

// Ensure environment variables are read.
require('../config/env')

const paths = require('../config/paths')
const spawn = require('react-dev-utils/crossSpawn')
let options = process.argv.slice(2)

if (options.indexOf('-p') === -1) {
  options = options.concat('-p', '6006')
}

const result = spawn.sync(
  'node',
  [require.resolve('@storybook/react/bin/index.js'), '-c', paths.ownStorybookConfig, ...options],
  { stdio: 'inherit' }
)
if (result.signal) {
  if (result.signal === 'SIGKILL') {
    console.log(
      'Storybook failed because the process exited too early. ' +
        'This probably means the system ran out of memory or someone called ' +
        '`kill -9` on the process.'
    )
  } else if (result.signal === 'SIGTERM') {
    console.log(
      'Storybook failed because the process exited too early. ' +
        'Someone might have called `kill` or `killall`, or the system could ' +
        'be shutting down.'
    )
  }
  process.exit(1)
}
process.exit(result.status)
