// Do this as the first thing so that any code reading it knows the right env.
process.env.BABEL_ENV = 'development'
process.env.NODE_ENV = 'development'

// Makes the script crash on unhandled rejections instead of silently
// ignoring them. In the future, promise rejections that are not handled will
// terminate the Node.js process with a non-zero exit code.
process.on('unhandledRejection', err => {
  throw err
})

// Ensure environment variables are read.
require('../config/env')

const spawn = require('react-dev-utils/crossSpawn')
let options = process.argv.slice(2)

// Check and modify command line arguments
if (options.includes('--healthwise')) {
  options = [...options, '@healthwise'].filter(option => option !== '--healthwise')
}

// Run npm-check-updates script from React App Config's package.json
const result = spawn.sync('npm', ['outdated', ...options], {
  stdio: 'inherit',
})
if (result.signal) {
  if (result.signal === 'SIGKILL') {
    console.log(
      'check-updates failed because the process exited too early. ' +
        'This probably means the system ran out of memory or someone called ' +
        '`kill -9` on the process.'
    )
  } else if (result.signal === 'SIGTERM') {
    console.log(
      'check-updates failed because the process exited too early. ' +
        'Someone might have called `kill` or `killall`, or the system could ' +
        'be shutting down.'
    )
  }
  process.exit(1)
}
process.exit(result.status)
