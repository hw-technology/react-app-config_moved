// Do this as the first thing so that any code reading it knows the right env.
process.env.BABEL_ENV = 'development'
process.env.NODE_ENV = 'development'

// Makes the script crash on unhandled rejections instead of silently
// ignoring them. In the future, promise rejections that are not handled will
// terminate the Node.js process with a non-zero exit code.
process.on('unhandledRejection', err => {
  throw err
})

// Ensure environment variables are read.
require('../config/env')

const chalk = require('chalk')
const fs = require('fs-extra')
const webpack = require('webpack')
const checkRequiredFiles = require('react-dev-utils/checkRequiredFiles')
const formatWebpackMessages = require('react-dev-utils/formatWebpackMessages')
const printBuildError = require('react-dev-utils/printBuildError')
const paths = require('../config/paths')
const checkConfigFiles = require('../util/checkConfigFiles')
const createWebpackConfig = require('../util/createWebpackConfig')

const options = process.argv.slice(2)
const isWatchMode = options.includes('--watch')

// Warn and crash if required files are missing
if (!checkRequiredFiles([paths.indexHtmlTemplate, paths.indexJs])) {
  process.exit(1)
}

// Merge default Webpack config with custom app config
let customConfig = {}
if (checkConfigFiles([paths.webpackConfigDev])) {
  customConfig = require(paths.webpackConfigDev)
}
const config = createWebpackConfig(require(paths.ownWebpackConfigDev), customConfig)

// Copy public folder to build folder
if (fs.existsSync(paths.public)) {
  fs.copySync(paths.public, paths.build, {
    dereference: true,
  })
}

// Create development build, and watch for changes
console.log('Creating development build...')
let compiler = webpack(config)
compiler.plugin('invalid', () => console.log(timeStamp(), 'Compiling...'))

// Run webpack.watch or webpack.run, depending on whether --watch option was included
if (isWatchMode) {
  const watching = compiler.watch({}, webpackHandler)
  const sigs = ['SIGINT', 'SIGTERM']
  sigs.forEach(sig => {
    process.on(sig, () => {
      console.log('Shutting down...')
      watching.close(() => process.exit())
    })
  })
} else {
  compiler.run(webpackHandler)
}

function webpackHandler(err, stats) {
  if (err) {
    printBuildError(err)
    return
  }

  const messages = formatWebpackMessages(stats.toJson({}, true))
  const warnings = messages.warnings

  if (messages.errors.length) {
    // Only keep the first error. Others are often indicative
    // of the same problem, but confuse the reader with noise.
    if (messages.errors.length > 1) {
      messages.errors.length = 1
    }
    printBuildError(new Error(messages.errors.join('\n\n')))
    return
  }

  if (warnings.length) {
    console.log(chalk.yellow(warnings.join('\n\n')))
    return
  }

  console.log(
    chalk.green(
      timeStamp() + ' Compiled successfully.' + (isWatchMode ? ' Watching for changes...' : '')
    )
  )
}

function timeStamp() {
  const now = new Date()
  const minutes = ('0' + now.getMinutes()).slice(-2)
  const seconds = ('0' + now.getSeconds()).slice(-2)
  return `[${now.getHours()}:${minutes}:${seconds}]`
}
