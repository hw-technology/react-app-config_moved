# Changelog

## 4.2.0 - February 15, 2019

- Moved remaining generated files to `/static` (facilitates CDN caching w/ varied TTLs per directory)

## 4.1.5 - February 14, 2019

- Temporarily downgraded `webpack-dev-server` to work with `webpack 3`

## 4.1.4 - January 25, 2019

- Version bump

## 4.1.3 - January 15, 2019

- Removed `npm-check-updates` package due to vulnerable dependencies. Updated `check-updates` script to use `npm outdated` instead.
- Updated some vulnerable dependencies.
- Removed optional `fsevents` dependency.

## 4.1.2 - November 28, 2018

- Only copy `public` folder on build if it exists

## 4.1.1 - November 27, 2018

- Removed `noopServiceWorkerMiddleware` in `webpackDevServer` to allow testing service workers locally

## 4.1.0 - October 8, 2018

- Added support for `public` folder to host static files

## 4.0.3 - October 8, 2018

- Added support for `beta` branch to publish pipeline

## 4.0.2 - September 25, 2018

- Added storybook addon knobs

## 4.0.1 - September 24, 2018

- Updated `@healthwise/eslint-plugin` to `^1.0.1`
- Updated Node engine to `>=8`

## 4.0.0 - September 10, 2018

- **BREAKING**: Moved ESLint configuration to `@healthwise/eslint-plugin` package
- **BREAKING**: Updated to use ESLint configuration from Create React App
- **BREAKING**: Implemented Prettier
- Added `lint` script
- Updated template and `init` script to include `lint-staged` command to automatically fix staged files on commit
- Added `gitignore` to `paths.js` configuration

## 3.4.0 - August 28, 2018

- Temporarily added `babel-preset-es2017` for async/await support (need to migrate to `@babel/preset-env` instead)
- Fixed invalid reference to `statuses` store in template
- Added missing template dependency on `redux-thunk`

## 3.3.0 - August 27, 2018

- Added Redux proprietary pattern & architecture to `/template`d app
- Added Redux Dev Tools

## 3.2.1 - August 17, 2018

- Prevent Storybook webpack config from clobbering aliases

## 3.2.0 - August 14, 2018

- Updated ESLint configuration to support ECMAScript 8

## 3.1.0 - July 30, 2018

- Added `check-updates` script to check for updated npm packages

## 3.0.0 - July 27, 2018

- Updated `@healthwise/theme` to `v4`
- Removed checks for deprecated theme variables
- Added `appTheme` alias to import app theme settings in UI Assets

## 2.3.6 - May 22, 2018

- Updated `README.md`

## 2.3.5 - May 17, 2018

- Updated Storybook Webpack config to use rules in `webpack.config.dev.js`

## 2.3.4 - May 17, 2018

- Removed destructuring assignment that isn't supported in older Node versions
- Updated wording in `init` script

## 2.3.3 - May 11, 2018

- Fixed syntax error

## 2.3.2 - May 11, 2018

- Fixed linting errors
- Added linting to prepush hook

## 2.3.1 - May 10, 2018

- Added `file-loader` to production webpack configuration
- Ensure `file-loader` is last when merging with custom webpack configs

## 2.3.0 - May 9, 2018

- Added `--coverage` flag to `test` script by default
- Added support for CSS modules in Jest config
- Updated Jest config to resolve modules in app src folder as well as `node_modules`

## 2.2.0 - May 4, 2018

- Removed `HtmlWebpackPlugin` minify options, using `webpack.optimize.UglifyJsPlugin` and `css-loader` instead

## 2.1.0 - May 3, 2018

- Added support for `alias` entries in app webpack configs, which will be appended to defaults

## 2.0.0 - April 17, 2018

- Updated `start` script to run Webpack Dev Server with hot module replacement
- Added `build-dev` script to bundle app in development mode, same behavior as previous `start` script
- Renamed `build` to `build-prod`, to make explicit the difference from `build-dev` script
- Added `start-storybook` script to run Storybook in development mode
- Added `build-storybook` script to build Storybook for deployment
- Removed `ExtractTextPlugin` for development builds
- Removed `CommonsChunkPlugin` for development builds
- Removed `vendor` bundle for development builds
- Added HTML minification for production builds
- Updated app template to use theme variables
- Updated app template's `README.md`
- Added simple Storybook story for `App` component
- Moved `App` component to `components` folder in template

## 1.0.1 - January 17, 2018

- Fixed starting template (sort of). Run `npm start` and open `build/index.html` manually in browser to view app.

## 1.0.0 - Initial release
