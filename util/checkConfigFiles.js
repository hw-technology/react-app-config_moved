const fs = require('fs')
const path = require('path')
const chalk = require('chalk')

function checkConfigFiles(files, logWarning = true) {
  let arrayFiles
  if (Array.isArray(files)) {
    arrayFiles = files
  } else {
    arrayFiles = [files]
  }

  let currentFilePath
  try {
    arrayFiles.forEach(filePath => {
      currentFilePath = filePath
      fs.accessSync(filePath, fs.F_OK)
    })
    return true
  } catch (err) {
    if (logWarning) {
      const dirName = path.dirname(currentFilePath)
      const fileName = path.basename(currentFilePath)
      console.log(chalk.yellow(`Could not find config file, ${fileName} in ${dirName}.`))
      console.log(chalk.yellow('Using default values from @healthwise/react-app-config.'))
    }
    return false
  }
}

module.exports = checkConfigFiles
