// Merges base Webpack config with a custom config. Plugins, loaders, and
// aliases defined in the custom config are appended to the default options.
// All other custom options will override the defaults.

module.exports = (baseConfig, customConfig) => {
  // Get loaders, so we can put them in proper order with customConfig loaders
  const eslintLoader = baseConfig.module.rules[0]
  let loaders = baseConfig.module.rules[1].oneOf
  const fileLoader = loaders.pop()

  return Object.assign(
    {},
    baseConfig,
    customConfig,
    {
      module: Object.assign({}, baseConfig.module, customConfig.module, {
        rules: [
          eslintLoader,
          {
            // Insert customConfig loaders before the final file-loader in baseConfig
            oneOf: [
              ...loaders,
              ...((customConfig.module && customConfig.module.rules) || []),
              fileLoader,
            ],
          },
        ],
      }),
    },
    {
      plugins: [...baseConfig.plugins, ...(customConfig.plugins || [])],
    },
    {
      resolve: Object.assign({}, baseConfig.resolve, customConfig.resolve, {
        alias: Object.assign(
          {},
          (baseConfig.resolve && baseConfig.resolve.alias) || {},
          (customConfig.resolve && customConfig.resolve.alias) || {}
        ),
      }),
    }
  )
}
