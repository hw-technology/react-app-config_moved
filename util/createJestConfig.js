// Merges base Jest config with a custom config. All options defined in the
// custom config will override the defaults.

module.exports = (baseConfig, customConfig) => {
  return Object.assign({}, baseConfig, customConfig)
}
