// List of 3rd party npm packages. These are bundled by Webpack separately
// from Healthwise source code.

module.exports = ['react', 'react-dom']
