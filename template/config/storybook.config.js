// App-specific Storybook configuration. Put global settings or global decorators
// for Storybook in the setup function. The loadStories function is required
// to load stories dynamically from the app.

const setup = () => {}

const loadStories = () => {
  const req = require.context('../src', true, /\.stories\.js$/)
  req.keys().forEach(req)
}

export { setup, loadStories }
