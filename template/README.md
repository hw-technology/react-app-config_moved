# This project was bootstrapped with react-app-config

Below you will find some information on how to perform common tasks.<br>

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will automatically reload if you make changes.<br>
You will also see any lint errors in the console.

### `npm run build-dev`

Builds the app for development, and watches for changes.<br>
Use this to develop the app with an existing back-end, such as a .NET app.

### `npm run build-prod`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

### `npm test`

Launches the test runner in interactive watch mode.

### `npm run start-storybook`

Runs Storybook in development mode, and watches for changes.<br>
Storybook will automatically reload if you make changes.

### `npm run build-storybook`

Builds Storybook as static files for deployment.
