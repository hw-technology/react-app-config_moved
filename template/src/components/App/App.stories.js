import React from 'react'
import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'

import App from './index'

storiesOf('App', module).add(
  'with defaults',
  withInfo(`
      Default rendering of \`App\` component
    `)(() => <App />)
)
