import React, { Component } from 'react'
import PropTypes from 'prop-types'

import styles from './App.css'

import Button from '@healthwise/button'
import HealthwiseLogo from '@healthwise/healthwise-logo'

class App extends Component {
  constructor(props) {
    super(props)

    this.addTask = this.addTask.bind(this)
  }

  componentDidMount() {
    this.props.getTasks()
  }

  addTask(e) {
    e.preventDefault()

    this.props.addTask(this.input.value)
    this.input.value = ''
  }

  removeTask(e, id) {
    e.preventDefault()

    this.props.removeTask(id)
    this.input.focus()
  }

  render() {
    const { tasks } = this.props

    return (
      <div className={styles.app}>
        <header className={styles.app_header}>
          <div className={styles.app_header_inner}>
            <div className={styles.app_logo}>
              <HealthwiseLogo />
            </div>
            <h1 className={styles.app_title}>Healthwise React App!</h1>
          </div>
        </header>

        <div className={styles.app_main}>
          <div>
            <p>
              To get started, edit <code>src/components/App/App.js</code> and save to reload.
            </p>
            <p>
              You can install components from UI Assets and use them here. Here is a button from UI
              Assets.
            </p>
            <Button rounded>UI Assets Button!</Button>
          </div>

          <div>
            <form onSubmit={this.addTask}>
              <input
                autoFocus
                ref={input => {
                  this.input = input
                }}
              />
              <Button rounded type="submit">
                Add task
              </Button>
            </form>
            {tasks && (
              <ul className={styles.ul}>
                {tasks.map((task, i) => (
                  <li key={task.id}>
                    <input type="checkbox" onClick={e => this.removeTask(e, task.id)} />
                    {task.title}
                  </li>
                ))}
              </ul>
            )}
          </div>
        </div>
      </div>
    )
  }
}

App.propTypes = {
  tasks: PropTypes.array,
  addTask: PropTypes.func,
  removeTask: PropTypes.func,
  getTasks: PropTypes.func,
}

export default App
