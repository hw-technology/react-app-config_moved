import React from 'react'
import { connect } from 'react-redux'

import { actions } from 'store/tasks'

import App from 'components/App'

const AppContainer = props => <App {...props} />

const mapStateToProps = store => {
  return {
    tasks: store.tasks,
  }
}

export default connect(
  mapStateToProps,
  actions
)(AppContainer)
