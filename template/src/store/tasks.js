import { createActions, createReducer } from '../utils'

const initialState = []

const actionList = {
  addTask: (state, action) => {
    return [
      ...state,
      {
        id: Math.floor(Math.random() * 100000) + 100,
        title: action.data,
      },
    ]
  },
  addTasks: (state, action) => {
    return state.concat(action.data)
  },
  removeTask: (state, action) => {
    return state.filter(task => task.id !== action.data)
  },
}

const actions = createActions(actionList)

const asyncActions = {
  getTasks: () => (dispatch, getState) => {
    fetch(`https://jsonplaceholder.typicode.com/todos`)
      .then(response => response.json())
      .then(json => {
        return dispatch(actions.addTasks(json.slice(0, 10)))
      })
  },
}

const combinedActions = { ...actions, ...asyncActions }
export { combinedActions as actions }
export default createReducer(initialState, actionList)
