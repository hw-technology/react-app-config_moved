import { combineReducers, createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension/logOnlyInProduction'

import tasks from './tasks'

const middleware = [thunk]

const reducer = combineReducers({
  tasks,
})

export default createStore(reducer, composeWithDevTools(applyMiddleware(...middleware)))
