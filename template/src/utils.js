export function createActions(actionList) {
  let actions = {}
  Object.keys(actionList).forEach(key => {
    actions[key] = data => ({ type: key, data: data })
  })
  return actions
}

export const createReducer = (initialState, actionList) => {
  return function(state = initialState, action = {}) {
    const type = action.type
    return (actionList[type] && actionList[type].call(this, state, action)) || state
  }
}
