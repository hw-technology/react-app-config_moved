module.exports = {
  presets: [['es2015', { modules: false }], 'es2017', 'react'],
  plugins: ['transform-object-rest-spread'],
  env: {
    development: {
      plugins: ['transform-react-jsx-source'],
    },
    test: {
      presets: ['es2015', 'react'],
    },
  },
}
