const path = require('path')
const fs = require('fs')
const checkConfigFiles = require('../util/checkConfigFiles')

// Make sure any symlinks in the project folder are resolved:
// https://github.com/facebookincubator/create-react-app/issues/637
const appDirectory = fs.realpathSync(process.cwd())
const resolveApp = relativePath => path.resolve(appDirectory, relativePath)
const resolveOwn = relativePath => path.resolve(__dirname, '..', relativePath)

const paths = {
  // Host application paths
  dotenv: resolveApp('.env'),
  build: resolveApp('build'),
  indexHtmlTemplate: resolveApp('src/index.template.html'),
  indexHtml: resolveApp('build/index.html'),
  indexJs: resolveApp('src/index.js'),
  packageJson: resolveApp('package.json'),
  packageVendor: resolveApp('config/package.vendor.js'),
  src: resolveApp('src'),
  healthwiseAssets: resolveApp('node_modules/@healthwise'),
  theme: resolveApp('config/theme.js'),
  jestConfig: resolveApp('config/jest.config.js'),
  webpackConfigDev: resolveApp('config/webpack.config.dev.js'),
  webpackConfigProd: resolveApp('config/webpack.config.prod.js'),
  storybookConfig: resolveApp('config/storybook.config.js'),
  nodeModules: resolveApp('node_modules'),
  gitignore: resolveApp('.gitignore'),
  public: resolveApp('public'),

  // Local configuration file paths
  ownBabelConfig: resolveOwn('config/babel.config.js'),
  ownEslintConfig: resolveOwn('config/eslint.config.js'),
  ownJestConfig: resolveOwn('config/jest/jest.config.js'),
  ownNodeModules: resolveOwn('node_modules'), // This is empty on npm 3
  ownPostcssConfig: resolveOwn('config/postcss.config.js'),
  ownStorybookConfig: resolveOwn('config/storybook'),
  ownWebpackConfigDev: resolveOwn('config/webpack.config.dev.js'),
  ownWebpackConfigProd: resolveOwn('config/webpack.config.prod.js'),
  ownWebpackDevServerConfig: resolveOwn('config/webpackDevServer.config.js'),
}

// Get custom path overrides from host application, if they exist
const customPathsConfig = resolveApp('config/paths.js')
let customPaths = {}
if (checkConfigFiles(customPathsConfig)) {
  customPaths = require(customPathsConfig)
}

module.exports = Object.assign({}, paths, customPaths)
