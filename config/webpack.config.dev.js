const webpack = require('webpack')
const AssetsPlugin = require('assets-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const eslintFormatter = require('react-dev-utils/eslintFormatter')
const paths = require('./paths')

module.exports = {
  context: paths.src,
  entry: [paths.indexJs],
  output: {
    path: paths.build,
    publicPath: '/',
    filename: 'static/js/[name].js',
    chunkFilename: 'static/js/[name].chunk.js',
    pathinfo: true,
  },
  resolve: {
    extensions: ['.jsx', '.js', '.json'],
    modules: [paths.src, 'node_modules', paths.nodeModules],
    symlinks: false,
    alias: {
      appTheme: paths.theme,
    },
  },
  target: 'web',
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        include: [paths.src],
        exclude: [/[/\\\\]node_modules[/\\\\]/],
        enforce: 'pre',
        use: [
          {
            loader: require.resolve('eslint-loader'),
            options: {
              formatter: eslintFormatter,
              eslintPath: require.resolve('eslint'),
              configFile: paths.ownEslintConfig,
              ignore: false,
              useEslintrc: false,
            },
          },
        ],
      },
      {
        // oneOf will traverse all following loaders until one matches
        // the requirements. When no loader matches it will fall
        // back to the file-loader at the end of the loader list.
        oneOf: [
          {
            test: /\.jsx?$/,
            include: [paths.src, paths.healthwiseAssets],
            exclude: [/\.test\.js$/],
            use: [
              {
                loader: require.resolve('babel-loader'),
                options: {
                  presets: [paths.ownBabelConfig],
                  babelrc: false,
                  cacheDirectory: true,
                },
              },
            ],
          },
          {
            test: /\.css$/,
            include: [paths.src, paths.healthwiseAssets],
            use: [
              require.resolve('style-loader'),
              {
                loader: require.resolve('css-loader'),
                options: {
                  modules: true,
                  importLoaders: 1,
                  localIdentName: '[path]-[name]--[local]',
                  sourceMap: true,
                },
              },
              {
                loader: require.resolve('postcss-loader'),
                options: {
                  config: {
                    path: paths.ownPostcssConfig,
                  },
                },
              },
            ],
          },
          // file-loader makes sure those assets get served by WebpackDevServer.
          // When you import an asset, you get its (virtual) filename.
          // In production, they would get copied to the build folder.
          // This loader doesn't use a "test" so it will catch all modules
          // that fall through the other loaders.
          {
            // Exclude js files to keep css-loader working, as it injects
            // its runtime that would otherwise be processed through file-loader.
            // Also exclude html and json extensions so they get processed
            // by webpack's internal loaders.
            exclude: [/\.(js|jsx)$/, /\.html$/, /\.json$/],
            loader: require.resolve('file-loader'),
            options: {
              name: 'static/media/[name].[hash:8].[ext]',
            },
          },
        ],
      },
    ],
  },
  plugins: [
    // Creates an assets file of all bundles created by Webpack
    new AssetsPlugin({
      path: paths.build,
      fullPath: false,
      prettyPrint: true,
    }),
    // Inserts dynamic bundle names into HTML or view template
    new HtmlWebpackPlugin({
      template: paths.indexHtmlTemplate,
      inject: false,
      filename: paths.indexHtml,
    }),
    // Sets NODE_ENV to development
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('development'),
    }),
  ],
  performance: {
    hints: false,
  },
}
