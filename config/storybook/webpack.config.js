const path = require('path')
const paths = require('../paths')
const checkConfigFiles = require('../../util/checkConfigFiles')
const createWebpackConfig = require('../../util/createWebpackConfig')
const ownConfig = require(paths.ownWebpackConfigDev)

module.exports = (baseConfig, env) => {
  // Merge parts of own Webpack config with Storybook's
  baseConfig.context = ownConfig.context
  baseConfig.resolve = ownConfig.resolve
  baseConfig.module = ownConfig.module

  // Modify JS loader to include Storybook directories.
  // JS Loader is first in the oneOf loader list.
  baseConfig.module.rules[1].oneOf[0].include = [
    path.resolve(__dirname),
    paths.storybookConfig,
    paths.src,
    paths.healthwiseAssets,
  ]

  // Add storybookConfig alias to be able to resolve Storybook files in host app
  baseConfig.resolve.alias['storybookConfig'] = paths.storybookConfig

  // Merge part of host app's Webpack config
  let customConfig = {}
  if (checkConfigFiles([paths.webpackConfigDev])) {
    const appConfig = require(paths.webpackConfigDev)
    if (appConfig.context) {
      customConfig.context = appConfig.context
    }
    if (appConfig.resolve) {
      customConfig.resolve = appConfig.resolve
    }
    if (appConfig.module) {
      customConfig.module = appConfig.module
    }
  }

  const config = createWebpackConfig(baseConfig, customConfig)

  return config
}
