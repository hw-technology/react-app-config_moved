import { configure } from '@storybook/react'
import { setDefaults } from '@storybook/addon-info'

// Import config file from app. This is aliased in webpack.config.js
import { setup, loadStories } from 'storybookConfig'

setup()
setDefaults({
  header: true,
  inline: false,
  source: true,
})
configure(loadStories, module)
