const path = require('path')
const paths = require('../paths')

const resolve = relativePath => path.resolve(__dirname, relativePath)

module.exports = {
  rootDir: paths.src,
  // Resolve modules in src folder as well as node_modules
  moduleDirectories: [paths.src, 'node_modules'],
  collectCoverageFrom: ['**/*.{js,jsx}'],
  coveragePathIgnorePatterns: ['/.*\\.test\\.js', '/.*\\.stories\\.js'],
  testRegex: '.*\\.test\\.js$',
  verbose: true,
  // Transform JS files with react-app-config's babel configuration
  // All other files with fileTransform
  transform: {
    '^.+\\.jsx?$': resolve('babelTransform.js'),
    '^(?!.*\\.(js|jsx|json|css)$)': resolve('fileTransform.js'),
  },
  // Process css imports with identity-obj-proxy to simulate CSS modules
  moduleNameMapper: {
    '^.+\\.css$': 'identity-obj-proxy',
  },
  transformIgnorePatterns: ['<rootDir>/node_modules/(?!(@healthwise)/).*/'],
}
