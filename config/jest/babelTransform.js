const babelJest = require('babel-jest')
const paths = require('../paths')
const babelConfig = require(paths.ownBabelConfig)

module.exports = babelJest.createTransformer(babelConfig)
