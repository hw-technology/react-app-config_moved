const paths = require('./paths')
const checkConfigFiles = require('../util/checkConfigFiles')

const appTheme = checkConfigFiles([paths.theme], false) ? require(paths.theme) : {}
const theme = require('@healthwise/theme')(appTheme)

const preserveCssVars = false
const supportedBrowsers = ['last 2 versions', 'IE >= 10']

module.exports = ctx => ({
  plugins: {
    'postcss-cssnext': {
      browsers: supportedBrowsers,
      features: {
        customProperties: {
          variables: theme,
          preserve: preserveCssVars,
          appendVariables: preserveCssVars,
        },
        autoprefixer: {
          cascade: false,
        },
      },
    },
    'postcss-flexbugs-fixes': {},
    // 'postcss-reporter': { throwError: true },
    // 'postcss-browser-reporter': ctx.env === 'production' ? {} : false
  },
})
