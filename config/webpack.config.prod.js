const webpack = require('webpack')
const AssetsPlugin = require('assets-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const eslintFormatter = require('react-dev-utils/eslintFormatter')
const checkConfigFiles = require('../util/checkConfigFiles')
const paths = require('./paths')

let vendorPackages = []
if (checkConfigFiles([paths.packageVendor])) {
  vendorPackages = require(paths.packageVendor)
}

module.exports = {
  context: paths.src,
  entry: {
    bundle: paths.indexJs,
    vendor: vendorPackages,
  },
  output: {
    path: paths.build,
    publicPath: '/',
    filename: 'static/js/[name].[chunkhash].js',
    chunkFilename: 'static/js/[name].[chunkhash].chunk.js',
    pathinfo: false,
  },
  resolve: {
    extensions: ['.jsx', '.js', '.json'],
    modules: [paths.src, 'node_modules', paths.nodeModules],
    symlinks: false,
    alias: {
      appTheme: paths.theme,
    },
  },
  target: 'web',
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        include: [paths.src],
        exclude: [/[/\\\\]node_modules[/\\\\]/],
        enforce: 'pre',
        use: [
          {
            loader: require.resolve('eslint-loader'),
            options: {
              formatter: eslintFormatter,
              eslintPath: require.resolve('eslint'),
              configFile: paths.ownEslintConfig,
              ignore: false,
              useEslintrc: false,
            },
          },
        ],
      },
      {
        // oneOf will traverse all following loaders until one matches
        // the requirements. When no loader matches it will fall
        // back to the file-loader at the end of the loader list.
        oneOf: [
          {
            test: /\.jsx?$/,
            include: [paths.src, paths.healthwiseAssets],
            exclude: [/\.test\.js$/],
            use: [
              {
                loader: require.resolve('babel-loader'),
                options: {
                  presets: [paths.ownBabelConfig],
                  babelrc: false,
                  compact: true,
                },
              },
            ],
          },
          {
            test: /\.css$/,
            include: [paths.src, paths.healthwiseAssets],
            use: ExtractTextPlugin.extract({
              fallback: require.resolve('style-loader'),
              use: [
                {
                  loader: require.resolve('css-loader'),
                  options: {
                    minimize: true,
                    modules: true,
                    importLoaders: 1,
                    localIdentName: '[name]--[local]--[hash:base64:5]',
                    sourceMap: true,
                  },
                },
                {
                  loader: require.resolve('postcss-loader'),
                  options: {
                    config: {
                      path: paths.ownPostcssConfig,
                    },
                  },
                },
              ],
            }),
          },
          // file-loader makes sure assets end up in the build folder.
          // When you import an asset, you get its filename.
          // This loader doesn't use a "test" so it will catch all modules
          // that fall through the other loaders.
          {
            // Exclude js files to keep css-loader working, as it injects
            // its runtime that would otherwise be processed through file-loader.
            // Also exclude html and json extensions so they get processed
            // by webpack's internal loaders.
            exclude: [/\.(js|jsx)$/, /\.html$/, /\.json$/],
            loader: require.resolve('file-loader'),
            options: {
              name: 'static/media/[name].[hash:8].[ext]',
            },
          },
        ],
      },
    ],
  },
  plugins: [
    // Saves imported CSS as separate files
    new ExtractTextPlugin({
      filename: 'static/css/[name]-[contenthash].css',
      allChunks: true,
    }),
    // Optimizes Webpack chunks in vendor bundles
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: Infinity,
    }),
    // Creates an assets file of all bundles created by Webpack
    new AssetsPlugin({
      path: paths.build,
      fullPath: false,
      prettyPrint: true,
    }),
    // Inserts dynamic bundle names into HTML or view template
    new HtmlWebpackPlugin({
      template: paths.indexHtmlTemplate,
      inject: false,
      filename: paths.indexHtml,
      chunks: ['vendor', 'bundle'],
    }),
    // Sets NODE_ENV to production
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production'),
    }),
    // Minify JS code
    new webpack.optimize.UglifyJsPlugin(),
    // Adds a copyright notice at the top of generated bundles
    new webpack.BannerPlugin({
      banner: 'Copyright ' + new Date().getFullYear() + ' Healthwise, Incorporated',
      entryOnly: false,
      exclude: /vendor.*\.js$/,
    }),
  ],
}
